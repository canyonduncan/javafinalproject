/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Canyon
 */
//Code the ability to Shuffle a deck and
//•	A dealCard method should remove the next card in the deck.
//•	A toString method for the deck should return the deck’s contents similar to the output displayed below.


public class Deck extends GroupOfCards {
    
    
    
    Deck(){
        
         
        for(int i = 0; i < 4; i++){
                Suit tempSuit = Suit.values()[i];
            for(int j=0; j<13; j++){
                
                Value tempVal = Value.values()[j];
                Card tempCard = new Card(tempVal, tempSuit);
                addCardToGroup(tempCard);
                
          
            }
            
        }
        
    }
     Deck(int numCards){
        
         
        for(int i = 0; i < 1; i++){
                Suit tempSuit = Suit.values()[i];
            for(int j=0; j<13; j++){
                
                Value tempVal = Value.values()[j];
                Card tempCard = new Card(tempVal, tempSuit);
                addCardToGroup(tempCard);
                
          
            }
            
        }
        
    }
    
    
    
    public void shuffle(){
        Collections.shuffle(cards);
        
    }
    
    public void dealCard(){
        cards.remove(cards.size() - 1);
        removeCardFromGroup(); // not removing the card but decramenting the size
    }
    
    public void dealCard(Hand dealto){
        dealto.addCardToGroup(cards.get(cards.size() - 1));
        cards.remove(cards.size() - 1);
        
    }
    
    
    
    
    
}
