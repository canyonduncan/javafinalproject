/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;



import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author Canyon
 */
public class Driver extends Application {

    ImageView cardBack = new ImageView();
    ImageView cardFront = new ImageView();
    ImageView cardMove = new ImageView();
    
    
    
        
    
    protected BorderPane getPane() {
        
        Image imageBack = new Image("image/back.png");
        Image imageFront = new Image("image/10C.png");
        Image image = new Image("image/10H.png");
        cardBack.setImage(imageBack);
        cardFront.setImage(imageFront);
        cardMove.setImage(image);
        HBox paneForCards = new HBox(10);
        
        
        StackPane cardFlipPane = new StackPane();
        
        
        

        ScaleTransition stHideFront = new ScaleTransition(Duration.millis(900), cardBack);
        stHideFront.setFromX(1);
        stHideFront.setToX(0);

        
        cardFront.setScaleX(0);

        ScaleTransition stShowBack = new ScaleTransition(Duration.millis(900), cardFront);
        stShowBack.setFromX(0);
        stShowBack.setToX(1);
        
        

        stHideFront.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                stShowBack.play();
            }
       });
       
        
        cardFlipPane.getChildren().addAll(cardBack, cardFront);
        
        
    
        Button refresh = new Button("Flip Card");
        Button moveCard = new Button("Move Card");
        
        
        paneForCards.getChildren().addAll(cardFlipPane, cardMove);
        paneForCards.setAlignment(Pos.CENTER);
        
        
        
        BorderPane pane = new BorderPane();
        
        pane.setTop(paneForCards);
        HBox paneForButton = new HBox(10);
        
        paneForButton.getChildren().addAll(refresh, moveCard);
        paneForButton.setAlignment(Pos.CENTER);
        //paneForButton.setStyle("-fx-border-color: red");
        
        pane.setCenter(paneForButton);
        
        pane.setPadding(new Insets(5,5,5,5));
        
        refresh.setOnAction(e-> {
            
            stHideFront.play();
            
            
        
        });
        
        moveCard.setOnAction(e-> {
            
            double x = cardMove.getX();
            double y = cardMove.getY();
            
            cardMove.setTranslateX(x += 50); 
            cardMove.setTranslateY(y += 50);
            
        });
        
        
        
        return pane;
        
    }

 
    
    
    @Override
    public void start(Stage primaryStage) {
        Scene scene = new Scene(getPane(), 1200, 500);
        //scene.setCamera(new PerspectiveCamera());
        primaryStage.setTitle("Quiz");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
