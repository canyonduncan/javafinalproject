/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author Canyon
 */
public class Screen1Controller implements Initializable {
    
    @FXML
    private Button playGameOfWar, playConnect4;
    
    @FXML
    private void handlePlayGameOfWar(ActionEvent event) throws IOException {
        
        Parent screen2Parent = FXMLLoader.load(getClass().getResource("Screen2.fxml"));
        Scene screen2Scene = new Scene(screen2Parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(screen2Scene);
        window.show();
    }
    
    @FXML
    private void handlePlayConnect4(ActionEvent event) throws IOException {
        Parent screen2Parent = FXMLLoader.load(getClass().getResource("Screen3.fxml"));
        Scene screen2Scene = new Scene(screen2Parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(screen2Scene);
        window.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
