/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

/**
 *
 * @author Canyon
 */


import java.util.ArrayList;
import java.util.Random;


public class Map {
	public enum team {red, yellow}
	private team[][] map; //Represents the game board
	private team teamPlaying;
	private team winningTeam;
	public int numMoves;
	private int numWinsRed;
	private int numWinsYellow;
	private int lastColumnPlayed;
	private int lastRowPlayed;
	private boolean win;
//	private boolean aiEnabled;
	//private Random random; //Currently used for AI
	public ArrayList<Integer> rowWins = new ArrayList<Integer>(); //A list containing the rows of the winning pieces in order
	public ArrayList<Integer> columnWins = new ArrayList<Integer>(); //A list containing the columns of the winning pieces in order
	//Tree<Map> aiTree;
	
	/**
	 * Default constructor
	 */
	public Map(){
		map = new team[7][6];
		numMoves = 0;
		numWinsRed = 0;
		numWinsYellow = 0;
		lastColumnPlayed = 0;
		lastRowPlayed = 0;
	}
	
	/**
	 * Copy constructor used for AI
	 * @param map
	 */
	public Map(Map map){
		this.map = map.map.clone();
		this.numMoves = map.numMoves;
		this.lastColumnPlayed = map.lastColumnPlayed;
		this.lastRowPlayed = map.lastRowPlayed;
	}
	
	/**
	 * Adds the playing teams piece to a specific column
	 * @param teamAdding - Decides what team the piece will be assigned to
	 * @param column - The column the piece will be added to 
	 */
	public void addpiece(team teamAdding, int column){
		if(column < 0 || column > 6){
			throw new IndexOutOfBoundsException("Map - addPiece(team, column)");
		}
		boolean foundOpenSpot = false;
		int i = 5;
		while(!foundOpenSpot && i >= 0){
			if(map[column][i] != null){
				i--;
			} else{
				foundOpenSpot = true;
				map[column][i] = teamAdding;
				lastColumnPlayed = column;
				lastRowPlayed = i;
				checkWin(teamAdding, column);
				numMoves++;
			}
		}
	}
	
	/**
	 * Checks to see if a certain team has won
	 * @param team - The team to check for a win
	 * @return true if the specified team has won
	 */
	public boolean checkWin(team team, int column){
		win = false;
		int row = 5;
		//Finding the row at which the last piece was added to
		for (int i = 5; i > 0; i--){
			if(map[column][i - 1] != null)
				row--;
		}
		
		//Check row for win
		checkRow(row,team);
		
		//Checking column for a win
		checkColumn(column,team);
		
		//Checking diagonals for a win
		checkDiagonals(column, row, team);
		
		//Setting the winning team and number of wins
		if(win){
			winningTeam = team;
			if(team == Map.team.red){
				numWinsRed++;
			} else{
				numWinsYellow++;
			}
		}
		
		return win;
	}
	
	/**
	 * Checks row for a win, and adds winning pieces to winning list
	 * Sets global variable win to true if a win is found
	 * @param row
	 * @param team
	 * @return true if win was detected, false if win was not detected
	 */
	private boolean checkRow(int row, team team) {
		
		//Used for adding winning pieces to list
		boolean sequenceStarted = false;
		
		//Checking row for a win
		int numInARow = 0;
		for(int i = 0; i < 7; i++){
			if(map[i][row] == team){
				numInARow++;
				rowWins.add(row);
				columnWins.add(i);
				if(numInARow == 4){
					win = true;
					sequenceStarted = true;
				}
			} else{
				if(sequenceStarted){
					break;
				}
				for(int j = 0; j < numInARow; j++){
					rowWins.remove(rowWins.size() -1);
					columnWins.remove(columnWins.size() - 1);
				}
				numInARow = 0;
			}
		}
		//Clearing the lists if there was not a win
		if(!win){
			rowWins.clear();
			columnWins.clear();
		}
		return win;
	}
	
	/**
	 * Checks current column for a win, and adds winning pieces to winning list
	 * @param column
	 * @param team
	 */
	private boolean checkColumn(int column, team team) {
		//Used for adding winning pieces to list
		boolean sequenceStarted = false;
		
		int numInARow = 0;
		for(int i = 5; i > -1; i --){
			if(map[column][i] == team){
				numInARow++;
				rowWins.add(i);
				columnWins.add(column);
				if(numInARow == 4){
					win = true;
					sequenceStarted = true;
				}
			} else{
				if(sequenceStarted){
					break;
				}
				for(int j = 0; j < numInARow; j++){
					rowWins.remove(rowWins.size() -1);
					columnWins.remove(columnWins.size() - 1);
				}
				numInARow = 0;
			}
		}
		//Clearing any pieces that are not part of the winning sequence
		if(!sequenceStarted){
			for(int j = 0; j < numInARow; j++){
				rowWins.remove(rowWins.size() -1);
				columnWins.remove(columnWins.size() - 1);
			}
		}
		return win;
	}
	/**
	 * Checks the diagonals for a win and adds any necessary winning pieces to 
	 * the winning lists
	 * @param column - The column being played	
	 * @param row - The row being played
	 * @param team - The team playing
	 */
	private boolean checkDiagonals(int column, int row, team team){
		int c; //Column - used for the following loops
		int r; //Row - used for the following loops
		int ur = 0; //The number of pieces up and to the right that are the same team
		int ul = 0; //The number of pieces up and to the left that are the same team
		int dr = 0; //The number of pieces down and to the right that are the same team
		int dl = 0; //The number of pieces down and to the left that are the same team
				
		//Checking up-right
		for(c = column, r = row; (c + 1 < 7) && (r - 1 > - 1); c++, r--){
			if(map[c + 1][r - 1] == team){
				ur++;
			} else{
				break;
			}
		}
		//Checking up-left
		for(c = column, r = row; (r - 1 > -1) && (c - 1 > -1); c--, r--){
			if(map[c - 1][r - 1] == team){
				ul++;
			} else{
				break;
			}
		}
		//Checking down-right
		for(c = column, r = row; (r + 1 < 6) && (c + 1 < 7); c++, r++){
			if(map[c + 1][r + 1] == team){
				dr++;
			} else{
				break;
			}
		}
		//Checking down-left
		for(c = column, r = row; (r + 1 < 6) && (c - 1 > -1); c--, r++){
			if(map[c - 1][r + 1] == team){
				dl++;
			} else{
				break;
			}
		}
		//Adding diagonals together
		if(ur + dl >= 3){
			win = true;
			rowWins.add(row);
			columnWins.add(column);
			//Adding winning pieces to the winning lists
			//Adding down-left
			for(c = column, r = row; (r + 1 < 6) && (c - 1 > -1); c--, r++){
				if(map[c - 1][r + 1] == team){
					columnWins.add(c - 1);
					rowWins.add(r + 1);
				} else{
					break;
				}
			}
			//Adding up-right
			for(c = column, r = row; (c + 1 < 7) && (r - 1 > - 1); c++, r--){
				if(map[c + 1][r - 1] == team){
					columnWins.add(c + 1);
					rowWins.add(r - 1);
				} else{
					break;
				}
			}
		} else if(dr + ul >= 3){
			win = true;
			rowWins.add(row);
			columnWins.add(column);
			//Adding winning pieces to the winning lists
			//Adding down-right
			for(c = column, r = row; (r + 1 < 6) && (c + 1 < 7); c++, r++){
				if(map[c + 1][r + 1] == team){
					columnWins.add(c + 1);
					rowWins.add(r + 1);
				} else{
					break;
				}
			}
			//Adding up-left
			for(c = column, r = row; (r - 1 > -1) && (c - 1 > -1); c--, r--){
				if(map[c - 1][r - 1] == team){
					columnWins.add(c - 1);
					rowWins.add(r - 1);
				} else{
					break;
				}
			}
		}
		return win;
	}
	
	/**
	 * Resets the map for a new game
	 */
	public void newGame(){
		for (int i = 0; i < 7; i++){
			for (int j = 0; j < 6; j++){
				map[i][j] = null;
			}
		}
		win = false;
		numMoves = 0;
		rowWins.clear();
		columnWins.clear();
	}
	
	/**
	 * Resets the number of wins for both teams
	 */
	public void resetNumWins(){
		numWinsRed = 0;
		numWinsYellow = 0;
	}
	
	/**
	 * Returns the number of wins for a given team
	 * @param team - the team whos number of wins will be returned
	 * @return the number of wins for a given team
	 */
	public int getNumWins(team team){
		int numWins = 0;
		if(team == Map.team.red){
			numWins = numWinsRed;
		} else{
			numWins = numWinsYellow;
		}
		return numWins;
	}
	/**
	 * Returns the team that has won if there has been a winner
	 * @return the winning team
	 */
	public team getWinningTeam(){
		return winningTeam;
	}
	
	/**
	 * Returns true if there has been a winner, false if otherwise.
	 * @return true or false
	 */
	public boolean hasWinner(){
		return win;
	}
	
	/**
	 * Checks to see if there has been a draw
	 * @return true if there has been a draw
	 */
	public boolean isDraw(){
		boolean isDraw = true;
		for(int i = 0; i < 7; i++){
			if(map[i][0] == null){
				isDraw = false;
			}
		}
		return isDraw;
	}
	
	/**
	 * Returns the team at the given location
	 * @param column - The column to check
	 * @param row - The row to check
	 * @return The team that has a piece at the given location - null if out of bounds or no team is at the given location
	 */
	public team getTeam(int column, int row){
		if(column > 6 || column < 0 || row > 5 || row < 0){
			return null;
		}
		
		return map[column][row];
	}
	
	/**
	 * Returns the team that is currently playing
	 * @return The team that is currently playing
	 */
	public team getTeamPlaying(){
		if(numMoves % 2 == 0){
			teamPlaying = Map.team.red;
		} else{
			teamPlaying = Map.team.yellow;
		}
		
		return teamPlaying;
	}
	
	/**
	 * Returns the column at which the last piece was played
	 * @return - lastColumnPlayed
	 */
	public int getLastColumnPlayed(){
		return lastColumnPlayed;
	}
	
	/**
	 * Returns the row at which the last piece was played
	 * @return - lastRowPlayed
	 */
	public int getLastRowPlayed(){
		return lastRowPlayed;
	}
	
	
}//End

