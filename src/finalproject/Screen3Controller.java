/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import finalproject.Map.team;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Canyon
 */
public class Screen3Controller implements Initializable {

    
    
    @FXML
    private Button goToScreen1;
    
    @FXML
    private BorderPane borderPane;
    
    @FXML
    private GridPane gridPane;
    
    @FXML 
    private Label teamPlayingLabel;
    
    private Circle[][] circleArray = new Circle[6][7];
    
    private Map map = new Map();
    
    
    
    
    //private Connect4Pane connect4Pane;
    int potentialColumnPlay; //The column at which the user has his/her mouse
    int potentialRowPlay; //The row at which the next piece would be dropped in potentialColumnPlay
    boolean isPlayedMove;
    boolean isPlaying;
    
    @FXML
    private void handleGoToScreen1(ActionEvent event) throws IOException {
        Parent screen1Parent = FXMLLoader.load(getClass().getResource("Screen1.fxml"));
        Scene screen1Scene = new Scene(screen1Parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(screen1Scene);
        window.show();
        
    }
    
//    @FXML 
//    private void handleMouseEntered(MouseEvent e){
//        Node source = (Node)e.getSource() ;
//        Integer colIndex = GridPane.getColumnIndex(source);
//        Integer rowIndex = GridPane.getRowIndex(source);
//        System.out.println("colIndex: " + colIndex);
//        //System.out.printf("Mouse entered cell [%d, %d]%n", colIndex.intValue(), rowIndex.intValue()); 
//    }
    
   
    private void addPane(int colIndex, int rowIndex) {
        StackPane pane = new StackPane();
        pane.setStyle("-fx-background-color: #0000b3;");
        Circle circle = new Circle(20);
        circleArray[rowIndex][colIndex] = circle;
        circle.setFill(Color.TRANSPARENT);
        circle.setStroke(Color.BLACK);
        //circle.setFill(javafx.scene.paint.Color.BLUE);
        pane.getChildren().add(circle);
        pane.setAlignment(Pos.CENTER);
        
        pane.setOnMouseClicked(e -> {
            isPlayedMove = true; 
            if(isPlaying){
                
            
                Map.team userTeam = map.getTeamPlaying();
                int columnPlaying = potentialColumnPlay;
                int rowPlaying = potentialRowPlay;
                Circle temp = circleArray[potentialRowPlay][potentialColumnPlay];
                if(map.getTeamPlaying() == team.red){
                    temp.setFill(javafx.scene.paint.Color.RED);
                }
                else{
                    temp.setFill(javafx.scene.paint.Color.YELLOW);
                }
                temp.setOpacity(1);
                map.addpiece(map.getTeamPlaying(), columnPlaying);
                teamPlayingLabel.setText("Team Playing: " + map.getTeamPlaying().toString().toUpperCase());
                checkForWinner();
            
            }
            
            
            
            
        
        });
        
        pane.setOnMouseEntered(e -> {
        //System.out.printf("Mouse enetered cell [%d, %d]%n", colIndex, rowIndex);
            potentialColumnPlay = colIndex;
            potentialRowPlay = -1;
            //Getting potential column
           
            //Getting potential row
            if(potentialColumnPlay < 7 && potentialColumnPlay > -1){
                    for(int i = 0; i < 6; i ++){
                            if(map.getTeam(potentialColumnPlay, i) == null){
                                    potentialRowPlay = i;
                                    //System.out.println("Potential Column = " + potentialColumnPlay);
                                    //System.out.println("Potential Row = " + potentialRowPlay);
                                    
                                    
                            }
                    }
                    
            }
            if(isPlaying){
                updateCircleOnMouseEnter(potentialColumnPlay, potentialRowPlay, map.getTeamPlaying());
            }
            
            
        });
        
        
        pane.setOnMouseExited(e -> {
            
            if(!isPlayedMove && isPlaying){
                updateCircleOnMouseExit(potentialColumnPlay, potentialRowPlay);
            }
            
            
            
            
            isPlayedMove = false;
            
        });
        
        gridPane.add(pane, colIndex, rowIndex);
    }
    

    
    
    public Node getNodeByRowColumnIndex (final int row, final int column, GridPane gridPane) {
    Node result = null;
    ObservableList<Node> childrens = gridPane.getChildren();

    for (Node node : childrens) {
        if(GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
            result = node;
            break;
        }
    }

    return result;
}
    
    
    public void updateCircleOnMouseEnter(int column, int row, team current){
        Circle temp = circleArray[row][column];
        Color tempColor;
        temp.setOpacity(.5);
        if(current == team.red){
            tempColor = Color.RED;
        }
        else{
            tempColor = Color.YELLOW;
        }
        temp.setFill(tempColor);
        
    }
    
    public void updateCircleOnMouseExit(int column, int row){
        
        Circle temp = circleArray[row][column];
        temp.setFill(Color.TRANSPARENT);
        temp.setOpacity(1.0);
        
        
    }
    
    
    public void checkForWinner(){
        int option = -1;
            if(map.isDraw()){
                    
                    //map.newGame();
                    teamPlayingLabel.setText("ITS A DRAW!!!");
                    isPlaying = false;
            }
            //Pop up when someone has won
            if(map.hasWinner()){
                    teamPlayingLabel.setText(map.getWinningTeam().toString().toUpperCase() + " WINS!!!!");
                    isPlaying = false;
                    //map.newGame();
            }
            if(option == 0){
                    
                    teamPlayingLabel.setText("Team Playing: " + map.getTeamPlaying().toString().toUpperCase());
                    
            } else if (option == 1 || option == 2){
                    
                    
                    
                    
            }
    }
  

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
      // connect4Pane = new Connect4Pane();
      // connect4Pane.gameBoardPane.setGridLinesVisible(true);
      // borderPane.setCenter(connect4Pane);
      
      gridPane.setGridLinesVisible(true);
      isPlaying = true;
      teamPlayingLabel.setText("Team Playing: " + map.getTeamPlaying().toString().toUpperCase());
      
      int numCols = 7;
      int numRows = 6;

//        for (int i = 0 ; i < numCols ; i++) {
//            ColumnConstraints colConstraints = new ColumnConstraints();
//            colConstraints.setHgrow(Priority.SOMETIMES);
//            gridPane.getColumnConstraints().add(colConstraints);
//        }
//
//        for (int i = 0 ; i < numRows ; i++) {
//            RowConstraints rowConstraints = new RowConstraints();
//            rowConstraints.setVgrow(Priority.SOMETIMES);
//            gridPane.getRowConstraints().add(rowConstraints);
//        }

        for (int i = 0 ; i < numCols ; i++) {
            for (int j = 0; j < numRows; j++) {
                addPane(i, j);
            }
        }
       
       
       
    }    
    
}
