/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;


import java.io.File;
import java.io.IOException;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;



/**
 *
 * @author Canyon
 */
public class Card implements Comparable<Card>{
    
    public Value num;
    public Suit suit;
    public String imgPath;

  
    
    
    Card(){}
    
    Card(Value num, Suit suit){
        this.setNum(num);
        this.setSuit(suit);
        this.setImgPath();
    }
    
    public Value getNum(){
        return this.num;
    }
    
    public Suit getSuit(){
        return this.suit;
    }
    public String getImgPath() {
        return this.imgPath;
    }
    
    public void setNum(Value num) {
        this.num = num;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }
    
    public void setImgPath() {
        String suit = this.suit.getName();
        Integer value = this.num.getNumber();
        suit = suit.substring(0,1);
        String file = "image/" + value.toString() + suit + ".png";
        this.imgPath = file;
    }

    @Override
    public String toString() {
        return num.getName() + " of " + suit.getName();
    }

        @Override
    public int compareTo(Card compare) {
        Value tempValue = ((Card)compare).getNum();
        int number = tempValue.getNumber();
        
        return number - this.num.getNumber();
        
    }
    
    public int findWinner(Card compare){
        Value tempValue = ((Card)compare).getNum();
        int otherCardNumber = tempValue.getNumber();
        int myCardNumber = this.num.getNumber();
        //System.out.println("Player 2: " + otherCardNumber);
        //System.out.println("Player 1: " + myCardNumber);
        if(myCardNumber > otherCardNumber){
            return 1;
        }
        else if(myCardNumber < otherCardNumber){
            return 2;
        }
        else{
            return 0;
        }
    }


   
    
}
