/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Canyon
 */
public class DisplayCards extends Application {

    ImageView card = new ImageView();
    ImageView card1 = new ImageView();
    ImageView card2 = new ImageView();
    ImageView card3 = new ImageView();
    Deck deck = new Deck();
        
    
    protected BorderPane getPane() {
        //create button
        deck.shuffle();
        HBox paneForCards = new HBox(10);
        
    
        Button refresh = new Button("Refresh");
        updateImages();
        
        paneForCards.getChildren().addAll(card, card1, card2, card3);
        paneForCards.setAlignment(Pos.CENTER);
        //paneForCards.setStyle("-fx-border-color: red");
        
        
        BorderPane pane = new BorderPane();
        
        pane.setTop(paneForCards);
        HBox paneForButton = new HBox(10);
        
        paneForButton.getChildren().add(refresh);
        paneForButton.setAlignment(Pos.CENTER);
        //paneForButton.setStyle("-fx-border-color: red");
        
        pane.setCenter(paneForButton);
        
        pane.setPadding(new Insets(5,5,5,5));
        
        
        refresh.setOnAction(e -> updateImages());
        
        
        return pane;
        
    }
    
    public void updateImages(){
        if(deck.getNumCards() >= 1){
        card.setImage(new Image(deck.getTopCard().getImgPath()));
        }
        
        if(deck.getNumCards() >= 1){
        card1.setImage(new Image(deck.getTopCard().getImgPath()));
        }
        if(deck.getNumCards() >= 1){
        card2.setImage(new Image(deck.getTopCard().getImgPath()));
        }
        if(deck.getNumCards() >= 1){
        card3.setImage(new Image(deck.getTopCard().getImgPath()));
        }
        
       
    }
    
    
    @Override
    public void start(Stage primaryStage) {
        Scene scene = new Scene(getPane(), 1200, 500);
        
        primaryStage.setTitle("Cards");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
