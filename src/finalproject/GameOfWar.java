/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

/**
 *
 * @author Canyon
 */
//implement the game interface
public class GameOfWar implements Game {
    final int numRounds = 100000;
    
    Hand player1 = new Hand();
    Hand player2 = new Hand();
    Hand pileOfCards = new Hand(); //= new GroupOfCards();
    Hand player1deltCards = new Hand();
    Hand player2deltCards = new Hand();
    Deck mainDeck = new Deck();
    //Deck mainDeck = new Deck(1);
    String winner;
    
  

    @Override
    public void initialize() {
        //mainDeck = new Deck();
        mainDeck.shuffle();
        //this.splitDeck();
    }
    
    public int playRound(){
        Card ply1 = player1.getTopCard();
        Card ply2 = player2.getTopCard();
        player1deltCards.addCardToGroup(ply1);
        player2deltCards.addCardToGroup(ply2);
        int betterCard = ply1.findWinner(ply2);
        
        return betterCard;
        
        
    }
    
    public void addCardsToWinner(int player){
        if(player == 2){
            //player 2 wins
            //System.out.println("Player 2 wins");
            player2.addGroupOfCardsToFront(player1deltCards);
            player2.addGroupOfCardsToFront(player2deltCards);
            
        }
        else if(player == 1){
            //System.out.println("Player 1 wins");
            player1.addGroupOfCardsToFront(player1deltCards);
            player1.addGroupOfCardsToFront(player2deltCards);
            
        }
        
    }

    @Override
    public void play() {
        
//        for(int i = 0; i < numRounds; i++){
//            
//            Card ply1Card = player1.getTopCard();
//            Card ply2Card = player2.getTopCard();
//            pileOfCards.addCardToGroup(ply1Card);
//            pileOfCards.addCardToGroup(ply2Card);
//            int betterCard = ply1Card.compareTo(ply2Card);
//            //System.out.println("better card = " + betterCard);
//            //System.out.println("player1 " + ply1Card.toString());
//            //System.out.println("Player2 " + ply2Card.toString());
//            if(betterCard < 0){
//                //player 2 wins
//                player2.addGroupOfCardsToFront(pileOfCards);
//            }
//            else if(betterCard > 0){
//                player1.addGroupOfCardsToFront(pileOfCards);
//            }
//            else{
//                if(player1.cards.size() > 1 && player2.cards.size() > 1){
//                    this.tieBreaker();
//                }
//                
//            }
//            if(player1.cards.size() == 0){
//                this.winner = "Player 2";
//                //this.displayWinner();
//                //System.out.println("Player 2 Wins!");
//                //System.out.println("Player two deck size: " + player2.cards.size());
//                //System.out.println("number of rounds: " + i);
//                break;
//            }
//            if(player2.cards.size() == 0){
//                this.winner = "Player 1";
//                //this.displayWinner();
//                //System.out.println("Player 1 Wins!");
//                //System.out.println("Player one deck size: " + player1.cards.size());
//                //System.out.println("number of rounds: " + i);
//                break;
//            }
//            
//        }
//        this.displayWinner();
       // System.out.println("No one is a winner today.");
    }
    
    public void splitDeck(){
        for(int i =0; i < mainDeck.getNumCards(); i++){
            if(i % 2 == 0){
                mainDeck.dealCard(player1);
            }
            else{
                mainDeck.dealCard(player2);
            }
            
        }
    }
    
    
    
    public void tieBreaker(){
            
            Card ply1Card = new Card();
            Card ply2Card = new Card();
            
            if(player1.cards.size() > 0) {
                 ply1Card = player1.getTopCard();
                 pileOfCards.addCardToGroup(ply1Card);
            }
            if(player2.cards.size() > 0) {
                 ply2Card = player2.getTopCard();
                 pileOfCards.addCardToGroup(ply2Card);
            }
            if(player1.cards.size() > 0) {
                 ply1Card = player1.getTopCard();
                 pileOfCards.addCardToGroup(ply1Card);
            }
            if(player2.cards.size() > 0) {
                 ply2Card = player2.getTopCard();
                 pileOfCards.addCardToGroup(ply2Card);
            }
            if(player1.cards.size() > 0) {
                 ply1Card = player1.getTopCard();
                 pileOfCards.addCardToGroup(ply1Card);
            }
            if(player2.cards.size() > 0) {
                 ply2Card = player2.getTopCard();
                 pileOfCards.addCardToGroup(ply2Card);
            }
            
            
            if(player1.cards.size() > 0) {
                 ply1Card = player1.getTopCard();
                 pileOfCards.addCardToGroup(ply1Card);
            }
            if(player2.cards.size() > 0) {
                 ply2Card = player2.getTopCard();
                 pileOfCards.addCardToGroup(ply2Card);
            }
            
            int betterCard = ply1Card.compareTo(ply2Card);
            
            
            
            if(betterCard < 0){
                //player 2 wins
                player2.addGroupOfCardsToFront(pileOfCards);
            }
            else if(betterCard > 0){
                player1.addGroupOfCardsToFront(pileOfCards);
            }
            else {
                this.tieBreaker();
            }
        
    }
    

    @Override
    public void displayWinner() {
        if(winner == null){
            System.out.println("No one is a winner today.");
        }
        else{
            System.out.println(winner + " wins!!!");
        }
        
    }
}
