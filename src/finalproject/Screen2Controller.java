/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates


When its come to the war. Make sure the player has enough cards in pile
and if not i need to make it so the winnings pile moves into the players pile 
so they can contiune the war

Add the check if there is a winner!

if player doenst have enough cards for war


 * and open the template in the editor.
 */
package finalproject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Stack;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Canyon
 */
public class Screen2Controller implements Initializable {

    
    final int DUR = 10;
    final int FLIPCARDDUR = 200;
    final double PAUSE = 0.5;
    GameOfWar gameOfWar = new GameOfWar();
    Image backOfCard = new Image("image/back.png");
    SequentialTransition seq = new SequentialTransition();
    
    ImageView player1FlipCard = new ImageView();
    ImageView player2FlipCard = new ImageView();
    
    Stack<ImageView> player1 = new Stack();
    Stack<ImageView> player2 = new Stack();
    
    Stack<ImageView> player1Winnings = new Stack();
    Stack<ImageView> player2Winnings = new Stack();
    //Stack warZone = new Stack();
    Stack<ImageView> player1WarZone = new Stack();
    Stack<ImageView> player2WarZone = new Stack();
    
    //TranslateTransition moveCardToPlayer1 = new TranslateTransition();
    
    //TranslateTransition movePlayer1ImageToWarZone = new TranslateTransition();
    //TranslateTransition movePlayer2ImageToWarZone = new TranslateTransition();
    //TranslateTransition movePlayer1WarZoneToPlayer1 = new TranslateTransition();
    //TranslateTransition movePlayer2WarZoneToPlayer1 = new TranslateTransition();
    //TranslateTransition movePlayer1WarZoneToPlayer2 = new TranslateTransition();
    //TranslateTransition movePlayer2WarZoneToPlayer2 = new TranslateTransition();
   
    SequentialTransition movePlayer1Cards = new SequentialTransition();
    SequentialTransition movePlayer2Cards = new SequentialTransition();
    
    SequentialTransition movePlayer1Winnings = new SequentialTransition();
    SequentialTransition movePlayer2Winnings = new SequentialTransition();
    

    public int stackPaneChildren = 0;
    
    @FXML
    private Button goToScreen1, btDealCards, btShowCards;
    
    //@FXML
    //private ImageView player1ImageView, player2ImageView;
    
    @FXML
    private StackPane stackPane;
    
    @FXML 
    private void dealCards(){
        gameOfWar.mainDeck.shuffle();
        seq.play();
        gameOfWar.splitDeck();
        btDealCards.setDisable(true);
        
    }
    
    
        
    @FXML AnchorPane anchorPane;
    
    @FXML
    private void handleGoToScreen1(ActionEvent event) throws IOException {
        Parent screen1Parent = FXMLLoader.load(getClass().getResource("Screen1.fxml"));
        Scene screen1Scene = new Scene(screen1Parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(screen1Scene);
        window.show();
        
    }
    
    @FXML 
    private void showCard(ActionEvent event){
        
        boolean isWinner = checkForWinner();
        if(isWinner){
            System.out.println("WINNER");
        }
        btShowCards.setDisable(true);
        

        movePlayer1Cards.getChildren().clear();
        movePlayer2Cards.getChildren().clear();
       
        
        
        
        int winner = gameOfWar.playRound();
        
        movePlayer1ToWarZone(true);
        movePlayer2ToWarZone(true);
        gameOfWar.addCardsToWinner(winner);
        
        
        
        
        if(winner == 1){
            movePlayer1WarZoneToPlayer1();
            movePlayer2WarZoneToPlayer1();
            
            movePlayer1Cards.play();
            movePlayer2Cards.play();
             
        }
        else if(winner == 2){
            movePlayer1WarZoneToPlayer2();
            movePlayer2WarZoneToPlayer2();
            
            movePlayer1Cards.play();
            movePlayer2Cards.play();
        }
        else {
           
          // movePlayer1Cards.play();
          // movePlayer2Cards.play();
//           if(player1.empty()){
//               addPlayer1WinningsToPile();
//           } 
           movePlayer1ToWarZone(false);
//           if(player2.empty()){
//               addPlayer1WinningsToPile();
//           }
           movePlayer2ToWarZone(false);
           
           movePlayer1ToWarZone(false);
           
           movePlayer2ToWarZone(false);
           
           movePlayer1ToWarZone(false);
           
           movePlayer2ToWarZone(false);
            movePlayer1Cards.play();
            movePlayer2Cards.play();
            
            
        }
     




        
        
    }
    
    
    public void movePlayer1ToWarZone(boolean showCard){
        
        if(player1.empty() && !player1Winnings.empty()){
            
            addPlayer1WinningsToPile(true);
        }
        else if(player1.empty() && player1Winnings.empty()){
            
            System.out.println("Player 2 wins!");
        }
        else {
            
        
            ImageView temp = (ImageView) player1.pop();




            TranslateTransition moveplayer1towarzone = new TranslateTransition();
            moveplayer1towarzone.setDuration(Duration.millis(100));
            moveplayer1towarzone.setByX(200 - (player1.size() * 2));
            moveplayer1towarzone.setByY(-250);
            moveplayer1towarzone.setCycleCount(1);
            moveplayer1towarzone.setNode(temp);

            movePlayer1Cards.getChildren().add(moveplayer1towarzone);
            temp.toFront();
            player1WarZone.add(temp);

            if(showCard){
                flipPlayer1Card(temp);
            }
        }
    }
    
    public void movePlayer2ToWarZone(boolean showCard){
        
        if(player2.empty() && !player2Winnings.empty()){
            addPlayer2WinningsToPile(true);
        }
        else if(player2.empty() && player2Winnings.empty()){
            
            System.out.println("Player 1 wins!");
        }
        else {
            ImageView temp = (ImageView) player2.pop();
        
    
            TranslateTransition moveplayer2towarzone = new TranslateTransition();
            moveplayer2towarzone.setDuration(Duration.millis(100));
            moveplayer2towarzone.setByX(-200 + (player2.size() * 2));
            moveplayer2towarzone.setByY(-250);
            moveplayer2towarzone.setCycleCount(1);
            moveplayer2towarzone.setNode(temp);

            movePlayer2Cards.getChildren().add(moveplayer2towarzone);
            temp.toFront();
            player2WarZone.add(temp);
            if(showCard){
                flipPlayer2Card(temp);
            }
        }
        
        
        
    }
    
    public void movePlayer1WarZoneToPlayer1(){
        
        
        for(ImageView temp : player1WarZone){
            
            TranslateTransition movePlayer1CardToPlayer1 = new TranslateTransition();
            movePlayer1CardToPlayer1.setDuration(Duration.millis(100));
            movePlayer1CardToPlayer1.setByX(-200);

            movePlayer1CardToPlayer1.setCycleCount(1);
            movePlayer1CardToPlayer1.setNode(temp);
            movePlayer1Cards.getChildren().add(movePlayer1CardToPlayer1);
            
            player1Winnings.add(temp);
        }
        player1WarZone.clear();
        
          
    }
    
    public void movePlayer2WarZoneToPlayer1(){
        
        
        for(ImageView temp : player2WarZone){
            TranslateTransition movePlayer2CardToPlayer1 = new TranslateTransition();
            movePlayer2CardToPlayer1.setDuration(Duration.millis(100));
            movePlayer2CardToPlayer1.setByX(-349);
            movePlayer2CardToPlayer1.setCycleCount(1);
            movePlayer2CardToPlayer1.setNode(temp);

            movePlayer2Cards.getChildren().add(movePlayer2CardToPlayer1);

            player1Winnings.add(temp);
        }
        player2WarZone.clear();
        
        
    }
    
    public void movePlayer1WarZoneToPlayer2(){
        
        for(ImageView temp : player1WarZone){
            TranslateTransition movePlayer1CardToPlayer2 = new TranslateTransition();
            movePlayer1CardToPlayer2.setDuration(Duration.millis(100));
            movePlayer1CardToPlayer2.setByX(349);//350

            movePlayer1CardToPlayer2.setCycleCount(1);
            movePlayer1CardToPlayer2.setNode(temp);
            movePlayer1Cards.getChildren().add(movePlayer1CardToPlayer2);

            player2Winnings.add(temp);
        }
        
        player1WarZone.clear();
        
          
    }
    
    public void movePlayer2WarZoneToPlayer2(){
        
        
        for(ImageView temp : player2WarZone){
            TranslateTransition movePlayer1CardToPlayer2 = new TranslateTransition();
            movePlayer1CardToPlayer2.setDuration(Duration.millis(100));
            movePlayer1CardToPlayer2.setByX(200);
            movePlayer1CardToPlayer2.setCycleCount(1);
            movePlayer1CardToPlayer2.setNode(temp);
            movePlayer2Cards.getChildren().add(movePlayer1CardToPlayer2);
            player2Winnings.add(temp);
        }
        
        player2WarZone.clear();
        
          
    }
    
    public void addPlayer1WinningsToPile(boolean inMovePlayer1){
        
        btShowCards.setDisable(true);
        
        movePlayer1Winnings.getChildren().clear();
        int size = player1Winnings.size();
        int offset = 0;
        for(int i = 0; i < size; i++){
            ImageView temp = (ImageView) player1Winnings.pop();
            temp.setImage(backOfCard);
            TranslateTransition moveWinnings = new TranslateTransition();
            moveWinnings.setDuration(Duration.millis(100));
            moveWinnings.setByX(offset);
            moveWinnings.setByY(250);
            moveWinnings.setCycleCount(1);
            moveWinnings.setNode(temp);
            if(inMovePlayer1){
                movePlayer1Cards.getChildren().add(moveWinnings);
            }
            else{
                movePlayer1Winnings.getChildren().add(moveWinnings);
            }
            
            temp.toFront();
            player1.add(temp);
            offset += 2;
        }
        
        if(!inMovePlayer1){
            btShowCards.setDisable(true);
            movePlayer1Winnings.play();
        }
        //movePlayer1Winnings.getS
        //movePlayer1Winnings.play();
        //while(movePlayer1Winnings.getStatus() == Animation.Status.RUNNING){
        //    System.out.println("running");
        //}
        
    }
    public void addPlayer2WinningsToPile(boolean inMovePlayer2){
        
        btShowCards.setDisable(true);
        
        movePlayer2Winnings.getChildren().clear();
        int size = player2Winnings.size();
        int offset = -1;
        for(int i = 0; i < size; i++){
            ImageView temp = (ImageView) player2Winnings.pop();
            temp.setImage(backOfCard);
            //temp.t
            TranslateTransition moveWinnings = new TranslateTransition();
            moveWinnings.setDuration(Duration.millis(100));
            moveWinnings.setByX(offset);
            moveWinnings.setByY(250);
            moveWinnings.setCycleCount(1);
            moveWinnings.setNode(temp);
            if(inMovePlayer2){
                movePlayer2Cards.getChildren().add(moveWinnings);
            }
            else{
                movePlayer2Winnings.getChildren().add(moveWinnings);
            }
            temp.toFront();
            player2.add(temp);
            offset-= 2;
            
            
        }
        
        if(!inMovePlayer2){
                btShowCards.setDisable(true);
                movePlayer2Winnings.play();
        }
        //movePlayer2Winnings.play();
        //while(movePlayer2Winnings.getStatus() == Animation.Status.RUNNING){
          //  System.out.println("running");
       // }
        
        
    }
    
    public boolean checkForWinner(){
        if(player1.empty() && player1Winnings.empty()){
            
            System.out.println("Player 2 wins");
            return true;
        }
        if(player2.empty() && player2Winnings.empty()){
            
            System.out.println("Player 2 wins");
            return true;
        }
        return false;
    }
    
    public void flipPlayer1Card(ImageView temp){
        player1FlipCard.toFront();
        temp.toFront();
        
        ScaleTransition stHideFront = new ScaleTransition(Duration.millis(FLIPCARDDUR), temp);
        stHideFront.setFromX(1);
        stHideFront.setToX(0);
      
        player1FlipCard.setOpacity(100);
        movePlayer1Cards.getChildren().add(stHideFront);
        
        
        Image cardImage = new Image(gameOfWar.player1deltCards.showTopCard().imgPath);
        player1FlipCard.setImage(cardImage);
        
        player1FlipCard.setScaleX(0);
        ScaleTransition stShowBack = new ScaleTransition(Duration.millis(FLIPCARDDUR), player1FlipCard);
        stShowBack.setFromX(0);
        stShowBack.setToX(1);
        stShowBack.setCycleCount(1);
        //stShowBack.setAutoReverse(true);
        movePlayer1Cards.getChildren().add(stShowBack);
        
        PauseTransition pause = new PauseTransition(Duration.seconds(PAUSE));
        movePlayer1Cards.getChildren().add(pause);
        
        
        
        ScaleTransition stHideFront2 = new ScaleTransition(Duration.millis(FLIPCARDDUR), player1FlipCard);
        stHideFront2.setFromX(1);
        stHideFront2.setToX(0);
       
        movePlayer1Cards.getChildren().add(stHideFront2);
       
        ScaleTransition stShowBack2 = new ScaleTransition(Duration.millis(FLIPCARDDUR), temp);
        stShowBack2.setFromX(0);
        stShowBack2.setToX(1);
        movePlayer1Cards.getChildren().add(stShowBack2);
        
        
    }
    public void flipPlayer2Card(ImageView temp){
        
        player2FlipCard.toFront();
        temp.toFront();
        
        ScaleTransition stHideFront = new ScaleTransition(Duration.millis(FLIPCARDDUR), temp);
        stHideFront.setFromX(1);
        stHideFront.setToX(0);
   
        player2FlipCard.setOpacity(100);
        movePlayer2Cards.getChildren().add(stHideFront);
        
        
        Image cardImage = new Image(gameOfWar.player2deltCards.showTopCard().imgPath);
        player2FlipCard.setImage(cardImage);
        
        player2FlipCard.setScaleX(0);
        ScaleTransition stShowBack = new ScaleTransition(Duration.millis(FLIPCARDDUR), player2FlipCard);
        stShowBack.setFromX(0);
        stShowBack.setToX(1);
        stShowBack.setCycleCount(1);
        //stShowBack.setAutoReverse(true);
        movePlayer2Cards.getChildren().add(stShowBack);
        
        //ParallelTransition pt = new ParallelTransition();
        //pt.getChildren().addAll(stHideFront, stShowBack);
        //movePlayer1Cards.getChildren().add(pt);
        PauseTransition pause = new PauseTransition(Duration.seconds(PAUSE));
        movePlayer2Cards.getChildren().add(pause);
        
        ScaleTransition stHideFront2 = new ScaleTransition(Duration.millis(FLIPCARDDUR), player2FlipCard);
        stHideFront2.setFromX(1);
        stHideFront2.setToX(0);
       
        movePlayer2Cards.getChildren().add(stHideFront2);
       
        ScaleTransition stShowBack2 = new ScaleTransition(Duration.millis(FLIPCARDDUR), temp);
        stShowBack2.setFromX(0);
        stShowBack2.setToX(1);
        movePlayer2Cards.getChildren().add(stShowBack2);
        
        
    }
    
    
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        movePlayer1Cards.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(player1.empty()){
                    btShowCards.setDisable(true);
                    addPlayer1WinningsToPile(false);
                    
                }
                else{
                    btShowCards.setDisable(false);
                    //System.out.println("Player 1:  ");
                    //System.out.println(gameOfWar.player1.toString());
                    
                }
                
            }
        });
        movePlayer2Cards.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                //if(player1.empty())
                //    addPlayer1WinningsToPile();
                if(player2.empty()){
                    btShowCards.setDisable(true);
                    addPlayer2WinningsToPile(false);
                }
                    //addPlayer2WinningsToPile();
                else if(movePlayer1Winnings.getStatus() != Animation.Status.STOPPED)
                    btShowCards.setDisable(false);
                //System.out.println("Player 1:  ");
                //System.out.println(gameOfWar.player2.toString());
            }
        });
        
        
        movePlayer1Winnings.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(movePlayer2Winnings.getStatus() == Animation.Status.STOPPED )
                    btShowCards.setDisable(false);
            }
        });
        movePlayer2Winnings.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                //if(player1.empty())
                //    addPlayer1WinningsToPile();
                if(movePlayer1Winnings.getStatus() == Animation.Status.STOPPED )
                    btShowCards.setDisable(false);
            }
        });

        
        
        
        for(int i = 0; i < gameOfWar.mainDeck.getNumCards(); i++){
            ImageView temp = new ImageView(backOfCard);
            temp.setFitHeight(200);
            temp.setFitWidth(125);
            
            if(i % 2 == 0){
                TranslateTransition moveCardToPlayer1 = new TranslateTransition();
                moveCardToPlayer1.setDuration(Duration.millis(DUR));
                moveCardToPlayer1.setByX(-275 + i);
                moveCardToPlayer1.setByY(175);
                moveCardToPlayer1.setCycleCount(1);
                moveCardToPlayer1.setNode(temp);
                seq.getChildren().add(moveCardToPlayer1);
                player1.add(temp);
                
            }
            else{
                TranslateTransition moveCardToPlayer2 = new TranslateTransition();
                moveCardToPlayer2.setDuration(Duration.millis(DUR));
                moveCardToPlayer2.setByX(275 - i);
                moveCardToPlayer2.setByY(175);
                moveCardToPlayer2.setCycleCount(1);
                moveCardToPlayer2.setNode(temp);
                seq.getChildren().add(moveCardToPlayer2);
                player2.add(temp);
            }
            
            
            stackPane.getChildren().add(temp);
            stackPaneChildren += 1;
            
            
            
        }
        
        player1FlipCard.setImage(backOfCard);
        player2FlipCard.setImage(backOfCard);
        
        player1FlipCard.setFitHeight(200);
        player1FlipCard.setFitWidth(125);
        
        player2FlipCard.setFitHeight(200);
        player2FlipCard.setFitWidth(125);
        
        player1FlipCard.setLayoutX(23);
        player1FlipCard.setLayoutY(-25);
        player1FlipCard.setManaged(false);
        player1FlipCard.setOpacity(0);
        
        player2FlipCard.setLayoutX(172);
        player2FlipCard.setLayoutY(-25);
        player2FlipCard.setManaged(false);
        player2FlipCard.setOpacity(0);
        
        stackPane.getChildren().add(player1FlipCard);
        stackPane.getChildren().add(player2FlipCard);
        //stackPane.setStyle("-fx-background-color: #0000b3;");
        
        
        
    }    
    
}

