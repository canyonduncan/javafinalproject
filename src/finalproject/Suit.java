/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

/**
 *
 * @author Canyon
 */
enum Suit {
    SPAIDS("Spaids"), 
    HEARTS("Heart"), 
    CLUBS("Clubs"), 
    DIAMONDS("Diamond");
    
    private final String suitName;
    
    
    private Suit(String name){
        this.suitName = name;
    }
    
    public String getName(){
        return this.suitName;
    }
}
