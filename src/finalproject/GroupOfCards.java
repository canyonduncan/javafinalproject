/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;
import java.util.ArrayList;
/**
 *
 * @author Canyon
 */

/**
 * 
 * @author Canyon
 * A GroupOfCards class that is a generic superclass for different groups of cards.  
 * Groups of cards should encompass a Deck and a Hand.  These should be separate classes 
 * with the appropriate variables and methods.

 */

 abstract class GroupOfCards {
    
    public ArrayList<Card> cards = new ArrayList<Card>();
    private int numCards = 0;
    
    

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public int getNumCards() {
        return numCards;
    }

    public void setNumCards(int numCards) {
        this.numCards = numCards;
    }
    
    public void addCardToGroup(Card card){
        this.cards.add(card);
        this.numCards++;
        
    }
    
    public void removeCardFromGroup(){
        this.numCards--;
        this.cards.remove(cards.size() - 1);
    }
    
    public Card getTopCard(){
        Card topCard = cards.get(cards.size() - 1);
        this.removeCardFromGroup();
        return topCard;
    }
    
    public Card showTopCard(){
        Card topCard = cards.get(cards.size() - 1);
        //this.removeCardFromGroup();
        return topCard;
    }

    public void addGroupOfCardsToFront(GroupOfCards groupToAdd){
        int groupToAddSize = groupToAdd.cards.size() - 1;
        for(int i = 0; i < groupToAddSize; i++){
            this.cards.add(i, groupToAdd.getTopCard());
        }
    }
    @Override
    public String toString() {
        String returnString = "";
        for(int i =0; i<cards.size();i++){
            Card temp;
            temp = cards.get(i);
            returnString += temp.toString() + "\n";
            
        }
        return returnString;
    }
    
}
