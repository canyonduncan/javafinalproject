/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 *
 * @author Canyon
 */
public class Connect4Pane extends Pane {
    Button playPieceButtons[];
    Map map;
    GridPane gameBoardPane = new GridPane();
    
	
    int potentialColumnPlay; //The column at which the user has his/her mouse
    int potentialRowPlay; 
    
    
    public Connect4Pane(){
        map = new Map();
	//initPlaypieceButtons();
	//initInfoPanel();
	//revalidate();
    }
    
    private void initPlaypieceButtons(){
		playPieceButtons = new Button[7];
		for (int i = 0; i < 7; i++){
			Button button = new Button();
			//button.setOpaque(false);
			//button.setContentAreaFilled(false);
			//button.setBorderPainted(false);
			//button.addMouseListener(new potentialPlayListener());
			playPieceButtons[i] = button;
		}
		
		// CREATING PANEL FOR IMAGE BUTTONS
		//gameBoardPanel = new JPanel(new GridLayout(1, 7));
		//gameBoardPanel.setOpaque(false);

		for (int i = 0; i < 7; i++) {
			gameBoardPane.add(playPieceButtons[i], i, 0);
		}
                //gameBoardPane.setMaxSize(300, 300);
                gameBoardPane.setPrefSize(500, 500);
                gameBoardPane.setHgap(20);
                gameBoardPane.setVgap(20);
                gameBoardPane.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		getChildren().add(gameBoardPane);
	}
    
    //private class potentialPlayListener impleme MouseEvent{
		
    //}
    
}
